drop table tbl_company;
drop table tbl_job;
drop table tbl_department;
drop table tbl_companyjob;
drop table tbl_companydepartment;

/* X relation a company */

CREATE TABLE tbl_company (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(128) NOT NULL
);
INSERT INTO tbl_company (id,name) VALUES (10, 'software & designers co');
INSERT INTO tbl_company (id,name) VALUES (20, 'website masters co');

/* Y relation #1, a job */

CREATE TABLE tbl_job (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(128) NOT NULL
);
INSERT INTO tbl_job (name) VALUES ('analist');
INSERT INTO tbl_job (name) VALUES ('programmer');
INSERT INTO tbl_job (name) VALUES ('template creator');
INSERT INTO tbl_job (name) VALUES ('html designer');

/* Y relation #2, a department */

CREATE TABLE tbl_department (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(128) NOT NULL
);
INSERT INTO tbl_department (name) VALUES ('administration');
INSERT INTO tbl_department (name) VALUES ('programming');
INSERT INTO tbl_department (name) VALUES ('reception');
INSERT INTO tbl_department (name) VALUES ('graphics');

/* XY relation (or AB relation) */

CREATE TABLE tbl_companyjob (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	company_id INTEGER NOT NULL,
	job_id INTEGER NOT NULL
);

CREATE TABLE tbl_companydepartment (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	company_id INTEGER NOT NULL,
	department_id INTEGER NOT NULL
);
