<h1>EYuiForm Demo</h1>

<p>This form is created using manual specification for pages,groups and fields.</p>

<p>Current selected user is: <?php echo $user->username.",".$user->email; ?></p>

<?php 
	$this->widget('ext.eyui.EYuiForm',array(
		'id'=>'form1',
		'label'=>'A Big Form',
		'model'=>EYuiFormDb::newModel($user),
		'showErrorResume'=>false,
		//'themeUrl'=>'themes',
		//'theme'=>'redmond',
		'jQueryUiEnabled'=>true,
		'jQueryControl'=>'tabs', // tabs or accordion
		'jQueryGroupControl'=>'', // use 'accordion' or leave blank
		
		// fielddes are field definitions, used later in each form field when needed.
		'fielddefs'=>array(
			'f1'=>array(
				// a specific pattern number
				'uicomponent'=>'textbox',
				'pattern'=>'/^([0-9]{5,8})$/',
				'patternMessage'=>'please review this field',
				'htmlOptions'=>array('maxlength'=>'5'),
				'help'=>'only 5 to 8 digits only',
			),
			'f2'=>array(
				// a simple name or lastname text box
				'uicomponent'=>'textbox',
				'pattern'=>'/^([A-Za-z]{3,10})$/',
				'help'=>'escriba solo letras',
				'htmlOptions'=>array('size'=>10,'maxlength'=>10),
			),
			'f3'=>array(
				// a text area field
				'uicomponent'=>'textarea',
				'pattern'=>'',
				'htmlOptions'=>array('rows'=>5,'cols'=>30),
			),
			'f4'=>array(
				// a combo box
				'uicomponent'=>'combobox',
				'prompt'=>array(0=>'-please select-'),
				'options'=>array(1=>'red',2=>'yellow',3=>'black'),
			),
			'f5'=>array(
				// simple boolean checkbox
				'uicomponent'=>'checkbox',
			),
		),// fielddefs
		
		
		// now, create pages and groups inside each page, finally field must be inserted into groups
		//
		'pages'=>array(
		
			
			'page1'=>array(	
				'label'=>'About You',
				'descr'=>'description for page 1',
				'groups'=>array(
					'group1'=>array(
						'label'=>'Your Basic Information',
						'descr'=>'descripcion aqui',
						'fields'=>array(
							'anynumber'=>array(
								'field'=>'f1','label'=>'Any Number',
								'required'=>true,'descr'=>'please write any number here',
								'default'=>'',
								'htmlOptions'=>array('style'=>'width: 100px; color: blue;'),
								//'help'=>'override field help using this field',
								),
							'firstname'=>array(
								'field'=>'f2','label'=>'First Name'	,
								'required'=>true,'descr'=>'Your First Name','default'=>''),
							'lastname'=>array(
								'field'=>'f2','label'=>'Last Name'	,
								'required'=>true,'descr'=>'Your Last Name','default'=>''),
							'separator1'=>'<hr/>',
							'address'=>array(
								'field'=>'f3','label'=>'Address'	,
								'required'=>true,'descr'=>'Please write your current address','default'=>''),
							'address2'=>array(
								'field'=>'f3','label'=>'Alternate Address'	,
								'required'=>true,'descr'=>'Have you another address ?','default'=>''),
						),
					),
					'group2'=>array(
						'label'=>'More About You',
						'descr'=>'please write more about you',
						'fields'=>array(
							'animals1'=>array(
								'field'=>'f5','label'=>'Do you like dogs ?',
								'required'=>false,'descr'=>'please specify'),
							'animals2'=>array(
								'field'=>'f5','label'=>'Do you like cats ?',
								'required'=>false,'descr'=>'please specify'),
							'separator1'=>'<hr/>',
							'colors'=>array(
								'field'=>'f4','label'=>'What is your preferred color ?',
								'required'=>false,'descr'=>'please specify'),
						),
					),
				),
			),
			

			
			'page2'=>array(	
				'label'=>'About Your Parents',
				'descr'=>'description for page 2',
				'groups'=>array(
					'group1'=>array(
						'label'=>'Your Mother',
						'descr'=>'descripcion aqui',
						'fields'=>array(
							// please note: 'firstname' and 'lastname' are currently in use
							// many time in this form, EYuiForm will threat it as different
							// fields, dont worry about specify different field names.
							'firstname'=>array(
								'field'=>'f2','label'=>'First Name'	,
								'required'=>true,'descr'=>'Your Mother First Name','default'=>''),
							'lastname'=>array(
								'field'=>'f2','label'=>'Last Name'	,
								'required'=>true,'descr'=>'Your Mother Last Name','default'=>''),
						),
					),
					'group2'=>array(
						'label'=>'Your Father',
						'descr'=>'descripcion aqui',
						'fields'=>array(
							// please note: 'firstname' and 'lastname' are currently in use
							// many time in this form, EYuiForm will threat it as different
							// fields, dont worry about specify different field names.
							'firstname'=>array(
								'field'=>'f2','label'=>'First Name'	,
								'required'=>true,'descr'=>'Your Father First Name','default'=>''),
							'lastname'=>array(
								'field'=>'f2','label'=>'Last Name'	,
								'required'=>true,'descr'=>'Your Father Last Name','default'=>''),
						),
					),
				),
			),
			
			
		),// end pages
	));
?>

