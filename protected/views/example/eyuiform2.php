<h1>EYuiForm Demo</h1>

<p>This form is created using <?php echo 
CHtml::link("eyui form editor",array('example/eyuiformeditor')) ?>.</p>


<?php 
	/*
		data can be exported too in CSV format using EYuiFormDataExport,
		please see also: actionDataExport
			
		(to access field values stored using EYuiForm please refer to README, item 3 or 4)
	*/
	echo "<ul><h4>CSV Export Demo:</h4>";
		echo "<li>".CHtml::link("export this user in CSV format",
			array('example/dataexport','userid'=>$user->id))."</li>";
		echo "<li>".CHtml::link("export all users in CSV format",array('example/dataexport'))."</li>";
	echo "</ul>";
?>

<p>Current selected user is: <?php echo $user->username.",".$user->email; ?></p>
<?php 
	// controller provides validationModel wich is an EYuiFormDb instance who will provide you
	// field values for current user (and provides extra validation too).
	$this->widget('ext.eyui.EYuiForm',array(
		'id'=>'form1',	// this is the 'form1'
		'label'=>'A Big Form',
		'model'=>EYuiFormDb::newModel($user),
		'showErrorResume'=>false,
		'jQueryUiEnabled'=>true,
		'jQueryControl'=>'tabs', // tabs or accordion
		'jQueryGroupControl'=>'', // use 'accordion' or leave blank
		'descriptionLocation'=>'title', // field description location: 'title' or 'visible'
		
		//	it loads the **form structure** (pages, groups and fields) of 'form1' stored using 
		//	a key named: 'mymodelid' (this is not the userid !!)
		'pages'=>EYuiFormEditorDb::model('mymodelid',"form1"),
	));
?>

