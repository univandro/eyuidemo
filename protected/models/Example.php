<?php
class Example extends CFormModel {
	public $userid;
	public $message;
	
	public function rules(){
		return array(
			array('userid','required'),
			array('userid','numerical','integerOnly'=>true),
			
			array('message','required'),
			array('message','length','max'=>50),
		);
	}
}